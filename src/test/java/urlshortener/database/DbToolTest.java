package urlshortener.database;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import urlshortener.entity.UrlsPair;
import urlshortener.to.UrlsPairTO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DbToolTest {

    private static final String A_FULL_URL = "www.google.com/longerThanHello";
    private static final String A_SHORT_URL = "www.google.com/hello";
    private static final Long AN_ID = 10L;

    @Mock
    private UrlsPairRepository repository;

    @InjectMocks
    public DbTool tool;

    @Test
    public void givenATo_WhenStoring_ShouldCallRepositorySave() {

        final UrlsPairTO to = new UrlsPairTO();

        tool.store(to);

        Mockito.verify(repository).save((UrlsPair)any());

    }

    @Test
    public void givenAShortUrl_WhenGettingUrlsPair_ShouldReturnCorrectPair() {

        String shortUrl = A_SHORT_URL;
        List<UrlsPair> pairs = getListUrlsPair();

        when(repository.findAll()).thenReturn(pairs);

        UrlsPair pair = tool.get(shortUrl);

        assertEquals(AN_ID, pair.getId());
        assertEquals(A_SHORT_URL, pair.getShortUrl());
        assertEquals(A_FULL_URL, pair.getFullUrl());

    }

    @Test
    public void givenAShortUrlAndNoPairRegistered_WhenGettingUrlsPair_ShouldReturnNull() {

        String shortUrl = A_SHORT_URL;
        List<UrlsPair> pairs = new ArrayList<>();

        when(repository.findAll()).thenReturn(pairs);

        UrlsPair pair = tool.get(shortUrl);

        assertNull(pair);


    }

    @Test
    public void givenAnIncorrectShortUrl_WhenGettingUrlsPair_ShouldReturnNull() {

        String shortUrl = "Random_Short";
        List<UrlsPair> pairs = getListUrlsPair();

        when(repository.findAll()).thenReturn(pairs);

        UrlsPair pair = tool.get(shortUrl);

        assertNull(pair);


    }

    @Test
    public void givenAShortUrl_WhenGettingUrl_ShouldReturnFullUrl() {

        String shortUrl = A_SHORT_URL;
        List<UrlsPair> pairs = getListUrlsPair();

        when(repository.findAll()).thenReturn(pairs);

        String url = tool.getUrl(shortUrl);

        assertEquals(A_FULL_URL, url);


    }


    @Test
    public void givenAShortUrlWithNoUrlsPair_WhenGettingUrl_ShouldReturnNull() {

        String shortUrl = A_SHORT_URL;
        List<UrlsPair> pairs = new ArrayList<>();

        when(repository.findAll()).thenReturn(pairs);

        String url = tool.getUrl(shortUrl);

        assertNull(A_FULL_URL, url);


    }

    @Test
    public void givenAShortUrl_WhenIsExisting_ShouldReturnTrue() {

        String shortUrl = A_SHORT_URL;
        List<UrlsPair> pairs = getListUrlsPair();

        when(repository.findAll()).thenReturn(pairs);

        boolean isExisting = tool.isExisting(shortUrl);

        assertTrue(isExisting);


    }

    @Test
    public void givenAShortUrlWithNoUrlsPair_WhenIsExisting_ShouldReturnFalse() {

        String shortUrl = A_SHORT_URL;
        List<UrlsPair> pairs = new ArrayList<>();

        when(repository.findAll()).thenReturn(pairs);

        boolean isExisting = tool.isExisting(shortUrl);

        assertFalse(isExisting);


    }

    private List<UrlsPair>  getListUrlsPair() {

        List<UrlsPair> pairs = new ArrayList<>();

        UrlsPair pair = new UrlsPair();
        pair.setId(AN_ID);
        pair.setShortUrl(A_SHORT_URL);
        pair.setFullUrl(A_FULL_URL);

        pairs.add(pair);

        return pairs;

    }

}