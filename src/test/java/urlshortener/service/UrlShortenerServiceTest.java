package urlshortener.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import urlshortener.database.DbTool;
import urlshortener.exception.UrlShortenerException;
import urlshortener.to.UrlsPairTO;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UrlShortenerServiceTest {

    private static final String AN_URL = "www.google.com/hello";
    private static final String A_SHORT_URL = "www.google.com/short";

    @Mock
    private UrlShortener shortener;

    @Mock
    private DbTool tool;

    @InjectMocks
    private UrlShortenerService service;

    @Test
    public void givenAnUrl_WhenShortenUrl_ThenRetrieveFullAndShortUrl() {
        String fullUrl = AN_URL;

        when(tool.isExisting(anyString())).thenReturn(false);
        when(shortener.shortenUrl(AN_URL)).thenReturn(A_SHORT_URL);


        UrlsPairTO to = service.shortenUrlAndStore(fullUrl);
        verify(tool).store(any());

        Assert.assertEquals(AN_URL, to.getFullUrl());
        Assert.assertEquals(A_SHORT_URL, to.getShortUrl());

    }

    @Test
    public void givenAnUrlAlReadyShortened_WhenShortenUrl_ThenRetrieveFullAndShortUrl() {
        String fullUrl = AN_URL;

        when(tool.isExisting(anyString())).thenReturn(true);
        when(shortener.shortenUrl(AN_URL)).thenReturn(A_SHORT_URL);

        UrlsPairTO to = service.shortenUrlAndStore(fullUrl);
        verify(tool, times(0)).store(any());

        Assert.assertEquals(AN_URL, to.getFullUrl());
        Assert.assertEquals(A_SHORT_URL, to.getShortUrl());

    }

    @Test
    public void givenAShortURL_WhenGetFullUrl_ShouldRetrieveFullURL() {
        String shortUrl = A_SHORT_URL;

        when(tool.getUrl(anyString())).thenReturn(AN_URL);

        String url = service.getFullUrl(shortUrl);

        Assert.assertEquals(AN_URL, url);

    }

    @Test(expected = UrlShortenerException.class)
    public void givenAShortURLNotRegistered_WhenGetFullUrl_ShouldThrowException() {
        String shortUrl = A_SHORT_URL;

        when(tool.getUrl(anyString())).thenReturn(null);

        service.getFullUrl(shortUrl);

    }

}
