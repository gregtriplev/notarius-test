package urlshortener.service;

import org.junit.Test;

import static org.junit.Assert.*;

public class UrlShortenerTest {

    private static final String A_SHORT_URL = "www.google.com/hello";
    private static final String A_LONG_URL = "www.google.com/hellohellohello";
    private UrlShortener shortener;

    @Test
    public void shortenUrl() throws Exception {

        shortener = new UrlShortener();

        String shortUrl = shortener.shortenUrl(A_SHORT_URL);

        assertEquals(A_SHORT_URL, shortUrl);
    }

    @Test
    public void shortenUrl2() throws Exception {

        shortener = new UrlShortener();

        String shortUrl = shortener.shortenUrl(A_LONG_URL);

        assertNotEquals(A_LONG_URL, shortUrl);
    }

}