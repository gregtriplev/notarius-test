package urlshortener.database;

import urlshortener.entity.UrlsPair;
import urlshortener.to.UrlsPairTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 *
 * DbTool is a class responsible for storing UrlsPairs, Retrieving Them or the full Url associated
 * or checking if a Short Url is already registered
 *
 */
@Service
public class DbTool {

    @Autowired
    private UrlsPairRepository repository;

    public DbTool(UrlsPairRepository repository) {
        this.repository = repository;
    }

    /**
     * Store an UrlsPair in the DB
     * @param to the UrlsPair Transfer Object whose data we want to store
     */
    public void store(UrlsPairTO to) {

        UrlsPair urlsPair = new UrlsPair();
        urlsPair.fromTO(to);

        repository.save(urlsPair);

    }

    /**
     *
     * Get an UrlsPair entity from a shortUrl
     *
     * @param shortUrl the short Url
     * @return the UrlsPair entity
     */
    public UrlsPair get(String shortUrl) {
        List<UrlsPair> pairs = repository.findAll();
        Optional<UrlsPair> optional = pairs.stream().filter(pair -> pair.getShortUrl().equals(shortUrl)).findFirst();
        return optional.isPresent() ? optional.get() : null;
    }

    /**
     *
     * Get the Full Url from a registered Short Url
     *
     * @param shortUrl the short Url
     * @return the Full Url
     */
    public String getUrl(String shortUrl) {
        UrlsPair pair = get(shortUrl);
        if (pair != null) {
            return pair.getFullUrl();
        }  else {
            return null;
        }
    }

    /**
     *
     * Check if a Short Url is already registered among the stored UrlsPairs
     *
     * @param shortUrl the short Url
     * @return true if the short url is found
     */
    public boolean isExisting(String shortUrl) {
        List<UrlsPair> pairs = repository.findAll();
        return pairs.stream().anyMatch(pair -> pair.getShortUrl().equals(shortUrl));
    }

}
