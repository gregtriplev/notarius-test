package urlshortener.database;

import urlshortener.entity.UrlsPair;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface UrlsPairRepository extends JpaRepository<UrlsPair, Long> {

}
