package urlshortener.to;

public class UrlsPairTO {

    private String fullUrl;
    private String shortUrl;

    public UrlsPairTO(){

    }

    public UrlsPairTO(String fullUrl, String shortUrl) {
        this.shortUrl = shortUrl;
        this.fullUrl = fullUrl;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }
}
