package urlshortener.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import urlshortener.exception.UrlShortenerException;
import urlshortener.service.UrlShortenerService;
import urlshortener.to.UrlsPairTO;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class UrlController {

    private static final String URL_REGEX = "^(http:\\/\\/|https:\\/\\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z\\/]+)?$";

    @Autowired
    private UrlShortenerService service;

    @RequestMapping("/shorturl")
    public UrlsPairTO shorten(@RequestParam String fromurl) {

        validateUrl(fromurl);

        UrlsPairTO to = service.shortenUrlAndStore(fromurl);
        return to;


    }

    @RequestMapping("/fullurl")
    public String retrieve(@RequestParam String fromurl) {
        return service.getFullUrl(fromurl);
    }

    private void validateUrl(String url) {
        Pattern patt = Pattern.compile(URL_REGEX);
        Matcher matcher = patt.matcher(url);
        if (!matcher.matches()) {
            throw new UrlShortenerException("Not Valid URL format : " + url);
        }
    }

}