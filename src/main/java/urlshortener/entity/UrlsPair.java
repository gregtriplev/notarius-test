package urlshortener.entity;


import urlshortener.to.UrlsPairTO;

import javax.persistence.*;

@Entity
@Table(name = "UrlsPair", schema = "Urls")
public class UrlsPair {

    @Id
    @GeneratedValue
    @Column(name="id")
    private Long id;

    @Column
    private String fullUrl;

    @Column
    private String shortUrl;


    public UrlsPair(){

    }

    public UrlsPair(String fullUrl, String shortUrl) {
        this.shortUrl = shortUrl;
        this.fullUrl = fullUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public void fromTO(UrlsPairTO to) {
        fullUrl = to.getFullUrl();
        shortUrl = to.getShortUrl();
    }

    public UrlsPairTO getTO() {
        UrlsPairTO to = new UrlsPairTO();
        to.setFullUrl(fullUrl);
        to.setShortUrl(shortUrl);
        return to;
    }
}
