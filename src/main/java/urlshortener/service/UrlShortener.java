package urlshortener.service;

import org.springframework.stereotype.Component;

/**
 *
 * This class contain the shortening algorythm
 * It gets everything after the domain and encodes id if it is longer than 10 characters
 *
 */
@Component
public class UrlShortener {

    /**
     *
     * Transforms a Full Url in a Short Version containing a maximum of 10 charcaters after the domain
     *
     * @param url the full url to transform
     * @return the short version of the full url
     */
    public String shortenUrl(String url) {

        int dotIndex = url.indexOf('.');
        int afterDomainIndex = url.substring(dotIndex).indexOf('/');
        String afterDomain = url.substring(dotIndex + afterDomainIndex + 1);
        String domain = url.substring(0,dotIndex +  afterDomainIndex + 1);

        String encodedPath = afterDomain;
        if (afterDomain.length() > 10) {
            encodedPath = "" + afterDomain.hashCode();
        }

        return domain + encodedPath;
    }

}
