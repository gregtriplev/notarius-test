package urlshortener.service;

import urlshortener.database.DbTool;
import urlshortener.exception.UrlShortenerException;
import urlshortener.to.UrlsPairTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * This service is responsible of calling the shortening tool and the storage service to register the Short urls.
 *
 */
@Service
public class UrlShortenerService {

    @Autowired
    private UrlShortener shortener;

    @Autowired
    private DbTool tool;

    /**
     *
     * Transforme the Full Url and store the data in the DB if it doesn't exist yet.
     *
     * @param fullUrl the full Url to tansform
     * @return        An UrlsPair Transfer Object with the full Url and The Short Url
     */
    public UrlsPairTO shortenUrlAndStore(String fullUrl) {

        String shortUrl = shortener.shortenUrl(fullUrl);

        UrlsPairTO to = new UrlsPairTO(fullUrl, shortUrl);
        if (!tool.isExisting(shortUrl)) {
            tool.store(to);
        }

        return to;

    }

    /**
     *
     * Retrieves the Full Url from the short Url
     *
     * @param shortUrl the Short url
     * @return The Full Url
     * @throws UrlShortenerException if no short url is registered
     */
    public String getFullUrl(String shortUrl) {
        String url = tool.getUrl(shortUrl);

        if(url == null) {
            throw new UrlShortenerException("Couldn't find any URL matching short version URL : " + shortUrl);
        }

        return url;
    }

}
