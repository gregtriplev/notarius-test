package urlshortener.exception;

public class UrlShortenerException extends RuntimeException {

    public UrlShortenerException(String message) {
        super(message);
    }
}
